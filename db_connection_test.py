import time
import pymssql

fc_path = 'env/connect_env.txt'
with open(fc_path, 'r') as fr :
    buf_lines = fr.readlines()
    row_server = buf_lines[0:1]
    row_user = buf_lines[1:2]
    row_password = buf_lines[2:3]
    row_database = buf_lines[3:4]
    server = row_server[0].replace('server = ', '')
    user = row_user[0].replace('user = ', '')
    password = row_password[0].replace('password = ', '')
    database = row_database[0].replace('database = ', '')
    print (server[:-2], user[:-2], password[:-2], database[:-2])

try :
    conn = pymssql.connect(server[:-2], user[:-2], password[:-2], database[:-2], timeout = 3)
    time.sleep(0.01)
    print 'db_step_1'
    cursor = conn.cursor()
    time.sleep(0.01)
    print 'db_step_2'
    cursor.execute('select getdate()')
    time.sleep(0.01)
    data = cursor.fetchone()
    print 'db_step_3'
    print data
    conn.commit()
    time.sleep(0.01)
    print 'db_step_4'
    conn.close()
    time.sleep(0.01)
    print 'db_step_5 connection closed'

except :
    print 'data is not input in db'
